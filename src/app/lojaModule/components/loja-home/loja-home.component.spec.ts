import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LojaHomeComponent } from './loja-home.component';

describe('LojaHomeComponent', () => {
  let component: LojaHomeComponent;
  let fixture: ComponentFixture<LojaHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LojaHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LojaHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
