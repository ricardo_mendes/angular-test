import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainLojaComponent } from './main-loja.component';

describe('MainLojaComponent', () => {
  let component: MainLojaComponent;
  let fixture: ComponentFixture<MainLojaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainLojaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainLojaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
