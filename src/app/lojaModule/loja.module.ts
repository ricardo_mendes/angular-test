import { NgModule } from '@angular/core';
import { MainLojaComponent } from './components/main-loja/main-loja.component';
import { CommonModule } from '../commonModule/common.module';
import { LojaHomeComponent } from './components/loja-home/loja-home.component';
import { LojaRoutesModule } from './loja.routes';

@NgModule({
  imports: [
    CommonModule,
    LojaRoutesModule
  ],
  declarations: [
    MainLojaComponent,
    LojaHomeComponent
  ],
  exports: [
    MainLojaComponent
  ]
})
export class LojaModule { }
