import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MainLojaComponent } from './components/main-loja/main-loja.component';

export const lojaRoutes: Routes = [
  { path: 'estoque', component: MainLojaComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(lojaRoutes)],
  exports: [RouterModule]
})

export class LojaRoutesModule { }
