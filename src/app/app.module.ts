import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutesModule } from './appRoutesModule/app-routes.module';

import { AppComponent } from './app.component';
import { LojaModule } from './lojaModule/loja.module';
import { CommonModule } from './commonModule/common.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AppRoutesModule,
    BrowserModule,
    LojaModule,
    CommonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
