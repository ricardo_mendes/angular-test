import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './../app.component';
import { LojaHomeComponent } from '../lojaModule/components/loja-home/loja-home.component';

import { lojaRoutes as LojaRoutes } from './../lojaModule/loja.routes';
import { MainLojaComponent } from '../lojaModule/components/main-loja/main-loja.component';

export const appRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: LojaHomeComponent },
  { path: 'manage', component: MainLojaComponent },
  // {
  //   path: 'home',
  //   component: LojaHomeComponent,
  //   children: [
  //       //  ...LojaRoutes
  //       { path: 'estoque', component: MainLojaComponent },
  //   ]
  // }
];

@NgModule({
  imports: [ RouterModule.forRoot(appRoutes) ],
  exports: [ RouterModule ]
})

export class AppRoutesModule { }
